<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title> Grocery List </title>

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <style type="text/css">
        h2 { font-size: 14pt; }
        h1 { font-size: 18pt; }
        .status { text-align: center; background: #ffffaa; }
        .empty-entry { background: #aaa; height: 20px; }
    </style>
  </head>
  <body>

    <? include_once( "process.php" ); ?>

    <div class="container">
        <h1>Grocery list</h1>

        <div class="new-item">
            <h2>Add new item</h2>
            <form method="post">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <label for="food">Food item:</label>
                        <input id="food" name="food" type="text" class="form-control"/>
                    </div>

                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="type">Section:</label>
                            <select class="form-control" id="type" name="type">
                            <option value="aisles"> Aisles </option>
                            <option value="baking"> Baking </option>
                            <option value="breakfast"> Breakfast </option>
                            <option value="canned"> Canned </option>
                            <option value="dairy"> Dairy </option>
                            <option value="frozen"> Frozen </option>
                            <option value="meat"> Meat </option>
                            <option value="produce"> Produce </option>
                            <option value="indian"> Indian </option>
                            <option value="pets"> Pets </option>
                            <option value="medicine"> Medicine </option>
                            <option value="household"> Household </option>
                            <option value="nonfood"> Non-food </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12">
                        <br>
                        <button type="submit" name="new" class="btn btn-primary form-control">Add</button>
                    </div>
                </div>
                
            </form>
        </div>

        <hr>

        <? if ( $status != "" ) { ?>
            <div class="status">
                <p><?=$status?></p>
            </div>
        <hr>
        <? } ?>

        <? $fields = array( "aisles", "baking", "breakfast", "canned", "dairy", "frozen", "meat", "produce", "indian", "pets", "medicine", "household", "nonfood" ); ?>

        <div class="row">
            <?  $i = 0;
                foreach ( $fields as $key=>$field ) { ?>
                <? if ( sizeof( $listArray[$field] ) > 0 ) { ?>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <form method="post">
                            <h2><?=$field?></h2>
                            <input type="text" name="type" value="<?=$field?>" style="display: none;">

                            <ul class="list-group">
                                <? foreach ( $listArray[$field] as $key => $value ) { ?>
                                <li class="list-group-item"> <button type="submit" class="btn btn-danger" name="remove" value="<?=$value?>">X</button> &nbsp; <em><?=$value?></em></li>
                                <? } ?>
                            </ul>
                        </form>
                    </div>
                <?
                $i++;
                } else { ?>
<!--
                    <div class="col-md-2 col-sm-6 col-xs-12">
                            <h2><?=$field?></h2>
                            <ul class="list-group empty-entry">
                            </ul>
                    </div>
-->
                <? } ?>

                <? if ( $i != 0 && $i % 6 == 0 ) { ?>
        </div>
        <div class="row">
                <? } ?>
            <? } ?>
        </div>
        
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
