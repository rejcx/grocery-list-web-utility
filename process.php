<?


    $listPath = "grocerylist.json";
    $listJson = file_get_contents( $listPath );
    $listArray = json_decode( $listJson, true );

    $status = "";

if ( sizeof( $_POST ) > 0 ) {
    // Add new item

    if ( isset( $_POST[ "new" ] ) ) {
        $foodText = $_POST[ "food" ];
        $foodType = $_POST[ "type" ];

        array_push( $listArray[ $foodType ], $foodText );

        $status .= "Added item to list. ";
    }

    // Delete item

    if ( isset( $_POST[ "remove" ] ) ) {
        $deleteText = $_POST[ "remove" ];
        $deleteType = $_POST[ "type" ];

        // Search for item
        $removeKey = -1;
        foreach( $listArray[ $deleteType ] as $key => $value ) {
            if ( $value == $deleteText ) {
                $removeKey = $key;
                break;
            }
        }

        if ( $removeKey != -1 ) {
            array_splice( $listArray[ $deleteType ], $removeKey, 1 );
            $status .= "Removed item from list. ";
        } else {
            $status .= "Unable to remove item from list. ";
        }
    }

    // Update list

    $listJson = json_encode( $listArray, JSON_PRETTY_PRINT );

    if ( file_put_contents( $listPath, $listJson ) ) {
        $status .= "Updated list successfully. ";

        // Reload
        $listPath = "grocerylist.json";
        $listJson = file_get_contents( $listPath );
        $listArray = json_decode( $listJson, true );
    } else {
        $status .= "Error updating list. ";
    }
    
}

?>
